package gitlab.saucelabs;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class SaucelabsConfig {
	
	WebDriver driver;
	
	@BeforeMethod
	public void setUp() throws MalformedURLException  {
				
		MutableCapabilities caps  = new MutableCapabilities();
		
		caps.setCapability("build", "Java-w3c-examples");
	    caps.setCapability("seleniumVersion", "3.141.59");
		caps.setCapability("username", "nitin.bhagat");
	    caps.setCapability("accessKey", "d7a5b957-df27-465a-be71-65036ecc2742");
	    caps.setCapability("tags", "Java-w3c-tests");
	    
	    DesiredCapabilities cap = new DesiredCapabilities();
	    cap.setCapability("sauce:options", caps);
    	cap.setCapability("platformName", "Android");
    //	cap.setCapability("device", "Emulator");
    	cap.setCapability("device", "Samsung Galaxy S9");
    //	cap.setCapability("device", "GoogleAPI Emulator");
	    cap.setCapability("os_version", "10.0");
	    cap.setCapability("app", "sauce-storage:myapplication.apk");
	    
	    driver = new RemoteWebDriver(new URL("https://ondemand.us-west-1.saucelabs.com:443/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
	}
	
	@AfterMethod()
	public void tearDown()  {
	//	driver.quit();
	}

}

package gitlab.saucelabs;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class TestNativeApp {

	public static AppiumDriver driver;

	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		
	//	File f = new File("app");
	//	File fs = new File(f,"selendroid-test-app-0.17.0.apk");
		
		DesiredCapabilities cap = new DesiredCapabilities();
		
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
		cap.setCapability("automationName", "UiAutomator2");
	//	cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		cap.setCapability("appPackage","io.selendroid.testapp");
		cap.setCapability("appActivity","io.selendroid.testapp.HomeScreenActivity");
		
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("com.android.permissioncontroller:id/continue_button")).click();
		
        Thread.sleep(2000);
		
		driver.findElement(By.id("android:id/button1")).click();
		
		Thread.sleep(2000);
		
		driver.closeApp();	
	}

	
	@Test
	public void test1()  {
		System.out.println("test print");
	}
}

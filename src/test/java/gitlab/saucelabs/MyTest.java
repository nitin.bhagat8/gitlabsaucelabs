package gitlab.saucelabs;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MyTest{
	
	WebDriver driver;

	@Parameters({ "env","platformName", "os_version", "device" })
	@Test
	public void check(String env, String platformName, String os_version, String device) throws MalformedURLException  {

		MutableCapabilities caps  = new MutableCapabilities();
		if(env.equals("Cloud"))  {
			caps.setCapability("username", "nitin.bhagat");
		    caps.setCapability("accessKey", "d7a5b957-df27-465a-be71-65036ecc2742");
		}		
	    
	    DesiredCapabilities cap = new DesiredCapabilities();
	    cap.setCapability("sauce:options", caps);
    	cap.setCapability("platformName", platformName);
    	cap.setCapability("device", device);
	    cap.setCapability("os_version", os_version);
	    
	    cap.setCapability("app", "storage:filename=Android.test.apk");
	    
	    driver = new RemoteWebDriver(new URL("https://ondemand.us-west-1.saucelabs.com:443/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		System.out.println("testing 3");
	//	EmailExtension1.send();
	}
}
